# Install_Netbox

This project was built to install Netbox (https://github.com/netbox-community/netbox). To run it:
1. Update the variables in the vars section.
2. If you are going to use the same box to download AND install netbox, run the playbook as normal.
3. If you are going to be installing netbox on a non-internet connected network, run this playbook with --tags download
    1. After the download is complete, tar the netbox_packages folder inside the root directory of your server and move it to the machine that will have netbox installed on it.
    2. Run the playbook but using --skip-tags download
4. If you are performing an upgrade of Netbox, simply run --tags upgrade
